<!-- Start Features46 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Image(s) with text and sublinks -->
@endif
<div class="features-46"  is="fir-features-46" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="features-46__wrap">
      <div class="features-46__images">
        <img src="https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg" alt="" class="features-46__main-img">
      </div>
      <div class="features-46__copy">
            <h4>
                {{ Fir\Utils\Helpers::checkMeta($meta, 
                    $subtitle, 
                    'Lorem ipsum dolor sit amet.') 
                }}
            </h4>
            <h2>
                {{ Fir\Utils\Helpers::checkMeta($meta, 
                    $title, 
                    'Lorem ipsum dolor sit amet.') 
                }}
            </h2>
            <p>
                {{ Fir\Utils\Helpers::checkMeta($meta, 
                    $text, 
                    'Lorem ipsum dolor sit amet.') 
                }}
            </p>

            @if($links)
            <div class="button__block">
                @foreach($links as $l)
                    <a href="{{ $l['link']['url'] }}" class="btn btn--blue">
                        @if($l['icon'])
                        <img src="{{ $l['icon']['url'] }}" alt="{{ $l['icon']['alt'] }}" class="fill-current w-4 h-4 mr-2">
                        @endif
                        <span>
                            {{ $l['link']['title'] }}
                        </span>
                    </a>
                @endforeach
            </div>
            @endif
      </div>
  </div>
</div>
<!-- End Features46 -->