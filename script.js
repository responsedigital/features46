class Features46 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures46()
    }

    initFeatures46 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features46")
    }

}

window.customElements.define('fir-features-46', Features46, { extends: 'div' })
