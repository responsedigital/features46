<?php

namespace Fir\Pinecones\Features46;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Features46',
            'label' => 'Pinecone: Features 46',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Image(s) with text and sublinks"
                ],
                [
                    'label' => 'Text',
                    'name' => 'textTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text'
                ],
                [
                    'label' => 'Sub Title',
                    'name' => 'subtitle',
                    'type' => 'text'
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea'
                ],
                [
                    'label' => 'Images',
                    'name' => 'imagesTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Images',
                    'name' => 'images',
                    'type' => 'repeater',
                    'layout' => 'table',
                    'min' => 1,
                    'max' => 6,
                    'button_label' => 'Add Image',
                    'sub_fields' => [
                        [
                            'label' => 'Image',
                            'name' => 'image',
                            'type' => 'image'
                        ],
                    ]
                ],
                [
                    'label' => 'Links',
                    'name' => 'linksTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Links',
                    'name' => 'links',
                    'type' => 'repeater',
                    'layout' => 'table',
                    'min' => 1,
                    'max' => 3,
                    'sub_fields' => [
                        [
                            'label' => 'Icon',
                            'name' => 'icon',
                            'type' => 'image'
                        ],
                        [
                            'label' => 'Link',
                            'name' => 'link',
                            'type' => 'link'
                        ],
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
